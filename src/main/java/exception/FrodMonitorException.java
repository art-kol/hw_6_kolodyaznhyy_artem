package exception;

public class FrodMonitorException extends Exception{

    public FrodMonitorException(String message) {
        super(message);
    }

}
