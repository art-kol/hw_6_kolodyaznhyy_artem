package HW6;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner in = new Scanner(System.in);
        TerminalImpl terminal = new TerminalImpl();

        while (!terminal.accessToTerminal) {
            System.out.println("Вставьте карту:");
            String cardNumber = in.next();
            System.out.println("Введите пин-код:");
            String pinCode = in.next();
            terminal.checkPinCode(cardNumber,pinCode);
        }

        while(true) {
            System.out.println("1. Проверить баланс");
            System.out.println("2. Перевод на карту другому клиенту");
            System.out.println("3. Положить деньги на счет");
            System.out.println("4. Выход"+"\n");
            int selectOp = in.nextInt();
            if (selectOp == 1) {
                System.out.println(terminal.checkClientBalance());
            } else if (selectOp == 2) {
                System.out.println("Введите сумму: ");
                int amount = in.nextInt();
                System.out.println("Введите номер карты получателя ");
                String recipientCard = in.next();
                terminal.doTransferClientToClient(amount, recipientCard);
            } else if (selectOp == 3) {
                System.out.println("Вставьте банкноту: ");
                int amount = in.nextInt();
                terminal.doDepositIntoYourAccount(amount);
            } else if (selectOp == 4) {
                break;
            }
        }

    }
}
