package HW6;

import exception.PinValidationException;

public class PinValidator {

    private final TerminalServer terminalServer;

    public PinValidator(TerminalServer terminalServer) {
        this.terminalServer = terminalServer;
    }

    boolean pinCodeValidation(String cardNumber, String pinCode) throws PinValidationException {
        if(terminalServer.getClientByCardnumberAndPincode(cardNumber, pinCode) == null){
            throw new PinValidationException("Incorrect pin code");
        } else {
            return true;
        }

    }
}
