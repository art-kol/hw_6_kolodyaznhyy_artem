package HW6;

import exception.FrodMonitorException;

public class FrodMonitor {

    private final TerminalServer terminalServer;

    public FrodMonitor(TerminalServer terminalServer) {
        this.terminalServer = terminalServer;
    }

    public boolean checkTransfer(Client client, int amount) throws FrodMonitorException {
        if(amount%100!=0){
            throw new FrodMonitorException("Transfer amount is not a multiple of 100");
        } else if(terminalServer.getClientWallet(client)>=amount){
            return true;
        } else {
            throw new FrodMonitorException("Insufficient funds to transfer");
        }
    }

    public boolean checkDeposit(int amount) throws FrodMonitorException {
        if(amount%100!=0){
            throw new FrodMonitorException("Deposit amount is not a multiple of 100");
        } else {
            return true;
        }
    }

}
