package HW6;

import exception.AccountIsLockedException;
import exception.AccountNotFoundException;
import exception.FrodMonitorException;
import exception.PinValidationException;

public class TerminalImpl implements Terminal{

    boolean accessToTerminal = false;
    private final TerminalServer terminalServer;
    private final PinValidator pinValidator;
    private final FrodMonitor frodMonitor;
    private Client sender;
    int accessAttempt = 0;

    public TerminalImpl() {
        terminalServer = TerminalServer.getTerminalServer();
        pinValidator = new PinValidator(terminalServer);
        frodMonitor = new FrodMonitor(terminalServer);

    }

    @Override
    public void checkPinCode(String cardNumber, String pinCode) throws InterruptedException {
        try {
            if(pinValidator.pinCodeValidation(cardNumber,pinCode)){
                accessToTerminal = true;
                sender = terminalServer.getClientByCardnumberAndPincode(cardNumber, pinCode);
                System.out.println("Доступ разрешен");
                System.out.println("Здравствуйте "+sender.getName()+"\n");
            }
        } catch (PinValidationException e) {
            try {
                accessToTerminal = false;
                accessAttempt++;
                if(accessAttempt == 3){
                    throw new AccountIsLockedException("Incorrect pin code. Your account has been blocked for 5 seconds");
                }
                System.out.println(e.getMessage()+"\n");
            } catch (AccountIsLockedException ex){
                System.out.println(ex.getMessage()+"\n");
                Thread.sleep(5000);
                accessAttempt = 0;
            }
        }
    }

    @Override
    public Integer checkClientBalance(){
        return sender.getWallet();
    }

    @Override
    public void doTransferClientToClient(int amount, String recipientCard) {
        try {
            Client recipient = terminalServer.getClientByCardNumber(recipientCard);
            if(recipient==null){
                throw new AccountNotFoundException("Account recepient not found");
            }

            if(frodMonitor.checkTransfer(sender, amount)){
                terminalServer.transferClientToClient(sender, recipient, amount);
                System.out.println("SUCCES: You transfer "+amount+" "+recipient.getName());
            }
        } catch (AccountNotFoundException | FrodMonitorException e) {
            System.out.println();
            System.out.println("FAILURE: "+e.getMessage()+"\n");
        }
    }

    @Override
    public void doDepositIntoYourAccount(int amount){
        try {
            if(frodMonitor.checkDeposit(amount)){
                sender.setWallet(sender.getWallet()+amount);
                System.out.println("SUCCES: You deposited in your account: "+amount+"\n");
            }
        } catch (FrodMonitorException e) {
            System.out.println("FAILURE: "+e.getMessage()+"\n");
        }
    }
}
