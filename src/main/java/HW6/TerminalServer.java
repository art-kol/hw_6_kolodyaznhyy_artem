package HW6;

import exception.AccountNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class TerminalServer {

    private static TerminalServer terminalServer;

    static List<Client> clients = new ArrayList<>();

    private TerminalServer(){

        clients.add(new Client("Иван",
                "Иванов",
                "Иванович",
                "1234", "5787", 700));

        clients.add(new Client("Михаил",
                "Сидоров",
                "Васильевич",
                "9101",
                "1213", 500));

    }

    public static TerminalServer getTerminalServer() {
        if (terminalServer == null) {
            terminalServer = new TerminalServer();
        }
        return terminalServer;
    }

    public Client getClientByCardnumberAndPincode(String cardNumber, String pinCode){
        for(Client client: clients){
            if(client.getCardNumber().equals(cardNumber) && client.getPinCode().equals(pinCode)){
                return client;
            }
        }
        return null;
    }

    public Client getClientByCardNumber(String cardNumber) throws AccountNotFoundException {
        for(Client client: clients){
            if(client.getCardNumber().equals(cardNumber)){
                return client;
            }
        }
        throw new AccountNotFoundException("Account recepient not found");
    }


    public Integer getClientWallet(Client client){
        return client.getWallet();
    }

    public void transferClientToClient(Client sendler, Client recipient, int amount){
        sendler.setWallet(sendler.getWallet()-amount);
        recipient.setWallet(recipient.getWallet()+amount);
    }

}
