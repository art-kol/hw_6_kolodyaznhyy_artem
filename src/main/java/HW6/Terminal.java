package HW6;

public interface Terminal {

    void checkPinCode(String cardNumber, String pinCode) throws InterruptedException;

    Integer checkClientBalance();

    void doTransferClientToClient(int amount, String recipientCard);

    void doDepositIntoYourAccount(int amount);
}
